import java.util.Scanner;

public class Mittelwert {
    
   public static void main(String[] args) {
	   
	  double x;
	  double y;
	  double m;

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
	  Scanner tastatur = new Scanner(System.in);
	  
	  System.out.print("Erste Zahl: ");
	  x = tastatur.nextDouble();
	  System.out.print("Zweite Zahl: ");
	  y = tastatur.nextDouble();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
	  m = berechneMittelwert(x, y);
	  

      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   
	static double berechneMittelwert(double x, double y)
	  {
		  return (x + y) / 2.0;
	  }

}
